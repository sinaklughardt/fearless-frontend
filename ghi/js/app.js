
function createCard(title, description, pictureUrl, starts, ends, location) {
    return `
    <div class="card shadow p-3 mb-5 bg-body rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
    ${starts} - ${ends}
  </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";
    const columns = document.querySelectorAll('.col');
    let colIndx = 0;
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error("Network response was not ok")


        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const start = new Date(details.conference.starts);
                    const end = new Date(details.conference.ends);
                    const location = details.conference.location.name
                    const pictureUrl = details.conference.location.picture_url;
                    const descriptionTag = document.querySelector('.card-text');
                    const html = createCard(title, description, pictureUrl, start.toLocaleDateString(), end.toLocaleDateString(), location);

                    const column = columns[colIndx % 3];

                    column.innerHTML += html;
                    colIndx = (colIndx + 1) % 3;


            }
            }
        }
    } catch(e) {
        console.log("There was an error:", e)
        const column = document.querySelector(".container")
        column.innerHTML += `
        <div class="alert alert-danger" role="alert">${e}</div>
        `;
    };

});
