import React, { useEffect, useState } from 'react';

function ConferenceForm() {

    const [locations, setLocations] = useState([]);
    const [name, setName] = useState("");
    const [starts, setStart] = useState("");
    const [ends, setEnd] = useState("");
    const [description, setDescription] = useState("");
    const [maxPresentations, setMaxPresentations] = useState("");
    const [maxAttendees, setMaxAttendees] = useState("")
    const [location, setLocation] = useState("")

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      };
      const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
      };
      const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
      };
      const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
      }
      const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
      }
      const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
      }
      const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
      }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';


        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
          console.log(data)
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName("");
            setStart("");
            setEnd("");
            setDescription("");
            setMaxAttendees("");
            setMaxPresentations("");
            setLocation("");
            console.log("hi")
        }
      }

    return (
        <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={name}></input>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleStartChange} placeholder="mm/dd/yyyy" required type="date" name="starts" id="starts" className="form-control" value={starts}></input>
                        <label htmlFor="starts">Starts</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEndChange} placeholder="mm/dd/yyyy" required type="date" id="ends" name="ends" className="form-control" value={ends}></input>
                        <label htmlFor="ends">Ends</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="description" className="form-label">Description</label>
                        <textarea onChange={handleDescriptionChange} rows="3" name="description" id="description" className="form-control" value={description}></textarea>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxPresChange} placeholder="Maximum presentations" required type="text" name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations}></input>
                        <label htmlFor="max_presentations">Max presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="text" name="max_attendees" id="max_attendees" className="form-control" value={maxAttendees}></input>
                        <label htmlFor="max_attendees">Maximum attendees</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleLocationChange} required id="location" name="location" className="form-select" value={location}>
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                        <option key={location.href} value={location.id}>
                            {location.name}
                        </option>
                        );
                    })};

                        <option value="">Choose a location</option>
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
            </div>
        </div>
      </div>
    </div>

    )
}
export default ConferenceForm
